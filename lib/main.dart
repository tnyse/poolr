import 'package:flutter/material.dart';
import 'screens/about.dart';
import 'screens/sched.2a.dart';
import 'screens/newpage.dart';
import 'screens/slivers.dart';
//import 'screens/widgets.dart';
import 'package:firebase_database/firebase_database.dart';

final FirebaseDatabase database = FirebaseDatabase.instance;
//yeah

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    //theme: new ThemeData(primaryColor: Color.fromRGBO(58, 66, 86, 1.0)),
    theme: new ThemeData(
        primaryColor: const Color(0xFF43a047),
        accentColor: const Color(0xFFffcc00),
        primaryColorBrightness: Brightness.dark,
      ),
  
  initialRoute: '/',
  routes: {
    '/': (context) => HomePage(),
    '/about': (context) => AboutPage(),
    '/schedule': (context) => Schedule(),
    '/new': (context) => NewPage(),
    '/sliver': (context) => SliverPage(),
  },
  ));
}

class HomePage extends StatefulWidget {
  
  @override
  HomePageState createState() => new HomePageState();
}

class HomePageState extends State<HomePage>{
  @override
  // Widget build(BuildContext context) {
    
// front page loader
    Widget build(BuildContext context) {
    
   return new Scaffold(
      // AppBar
      appBar: new AppBar(
        // Title
        title: new Text("Home Page"),
        // App Bar background color
        backgroundColor: Colors.red,
      ),
      // Body
      body: new Container(
        // Center the content
        child: new Center(
          child: new Column(
            // Center content in the column
            mainAxisAlignment: MainAxisAlignment.center,
            // add children to the column
            children: <Widget>[
              // Text
              new Text(
                "Home Page\n\nClick on below icon to goto next Page",
                // Setting the style for the Text
                style: new TextStyle(fontSize: 20.0,),
                // Set text alignment to center
                textAlign: TextAlign.center,
              ),
              // Icon Button
              new IconButton(
                icon: new Icon(
                  Icons.money_off,
                  color: Colors.blue,
                ),
                // Execute when pressed
                onPressed: () {
                  // use the navigator to goto a named route
                  // Navigator.of(context).pushNamed('/about');

                  Navigator.pushNamed(context, '/about');
                  debugPrint("dgc going to about page");
                },
                // Setting the size of icon
                iconSize: 80.0,
              ),
              new IconButton(
                icon: new Icon(
                  Icons.movie,
                  color: Colors.green,
                ),
                // Execute when pressed
                onPressed: () {
                  // use the navigator to goto a named route
                  // Navigator.of(context).pushNamed('/about');

                  //Navigator.pushNamed(context, '/new');
                  Navigator.push(context, new MaterialPageRoute(
                    builder: (context) =>
                      new Schedule())
                    );
                  debugPrint("dgc going to schedule page");
                },
                // Setting the size of icon
                iconSize: 80.0,
              ),
             // Icon Button
              new IconButton(
                icon: new Icon(
                  Icons.notifications_off,
                  color: Colors.red,
                ),
                // Execute when pressed
                onPressed: () {
                  // use the navigator to goto a named route
                  // Navigator.of(context).pushNamed('/about');

                  // Navigator.pushNamed(context, '/sliver');
                  Navigator.push(context, new MaterialPageRoute(
                    builder: (context) =>
                      new SliverPage())
                    );
                  debugPrint("dgc going to slivers page");
                },
                // Setting the size of icon
                iconSize: 80.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

}