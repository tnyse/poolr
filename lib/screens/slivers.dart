
import 'package:flutter/material.dart';


class SliverPage extends StatelessWidget {
  static const String routeName = "/sliver";
  //ScheduleState createState() => new ScheduleState();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      // AppBar
      // appBar: new AppBar(
      //   // Title
      //   title: new Text("sliverPage"),
      //   // App Bar background color
      //   backgroundColor: Colors.blue,
      // ),
      // Body
      body: new CustomScrollView(
  slivers: <Widget>[
    const SliverAppBar(
      pinned: true,
      expandedHeight: 250.0,
      flexibleSpace: FlexibleSpaceBar(
        title: Text('Sliver Schedule'),
      ),
    ),
// here
    SliverFixedExtentList(
      itemExtent: 50.0,
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return Container(
            alignment: Alignment.center,
            color: Colors.lightGreen[100 * (index % 4)],
            child: Text('list item $index'),
          );
        },
      ),
    ),

  ],
)


    );
  }
}

