import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
//import 'package:poolr/screens/game.dart';

class Schedule extends StatefulWidget {
  static const String routeName = "/schedule";
  ScheduleState createState() => new ScheduleState();
}

List data;

// At the top level:
// enum SingingCharacter { $home, $away }
enum SingleGame { $home, $away }

class ScheduleState extends State<Schedule> {
SingleGame _gamePick =  SingleGame.$home;

  Future<String> getJson() async {
    var response = await http.get(
       Uri.encodeFull("https://sheetdb.io/api/v1/red9m0vfm55hy"),
      headers: {
        "Accept":"application/json"
      }
    );

    this.setState((){
      data =json.decode(response.body);
      });

    print("schedule imported successfully!"); 
  } //

  @override
  void initState (){
    this.getJson();
  }

  @override
      Widget build(BuildContext context) {

        var scaffold = new Scaffold(
          appBar: new AppBar(
            title: new Text('Schedule'),
            backgroundColor: Colors.green,
          ),
          body: 

          new ListView.separated(
            separatorBuilder: (context, index) => Divider(
                  color: Colors.green,
                ),
            itemCount: data ==null ? 0 :data.length,
            itemBuilder: (BuildContext context, i){

          // radiolisttile  ***
                return new Column(
                  children:  <Widget>[
                     RadioListTile<SingleGame>(
                      title: new Text ("${data[i]["home"].toUpperCase()}"),
                      value: SingleGame.$home,
                      groupValue: _gamePick,
                      onChanged: (SingleGame value) { setState(() { _gamePick = value; }); },
                    ),
                    RadioListTile<SingleGame>(
                      title: new Text ("${data[i]["away"].toUpperCase()}"),
                      value: SingleGame.$away,
                      groupValue: _gamePick,
                      onChanged: (SingleGame value) { setState(() { _gamePick = value; }); },
                    ),
                  ],
                );

          //regular list tiles ***
              // return new RadioListTile(
              //   // leading: Icon(Icons.shopping_cart),
              //   // trailing:Icon(Icons.music_video),
              //   title: new Text("${data[i]["home"].toUpperCase()} at ${data[i]["away"].toUpperCase()}"),
              //   subtitle: Text("Winner: ${data[i]["winner"].toUpperCase()} : ${data[i]["score"]}"),
              // );      

            },
          ),         
        );
       return scaffold;
      } //widget

} //class
