import 'package:flutter/material.dart';

class NewPage extends StatelessWidget {
  static const String routeName = "/new";

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      // AppBar
      appBar: new AppBar(
        // Title
        title: new Text("New Page"),
        // App Bar background color
        backgroundColor: Colors.blue,
      ),
      // Body
      body: new Container(
        // Center the content
        child: new Center(
          // here you go
        ),
      ),
    );
  }
}