import 'package:flutter/material.dart';
import './quiz1.dart' ;

void main() {
    runApp(
      new MaterialApp(
        home: AnimalQuiz(),
    )
  ); 
}

class AnimalQuiz extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return new AnimalQuizState();
      }
}


    
class AnimalQuizState extends State<AnimalQuiz> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Pick Em!"),
        backgroundColor: Colors.blue,
      ),


      body: new Container(
        //bg image for container
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("images/bg.jpg"),
                fit: BoxFit.cover
            )
        ),
        // margin: const EdgeInsets.all(15.0),
        child: new Column(

          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

        Padding(
        padding: EdgeInsets.all(30.0),
        child:

            new Text("NFL Week 1!",
              style: new TextStyle(
                  fontSize: 40.0,
                  color: Colors.white
              ),),),

            Padding(
              padding: EdgeInsets.all(40.0),
              child:

              new MaterialButton(
                minWidth: 100.0,
                height: 60.0,
                color: Colors.lightBlue,
                onPressed: startQuiz,
                child:
                new Text("Let's Play!",
                  style: new TextStyle(
                      fontSize: 40.0,
                      color: Colors.white
                  ),),
              )   )



          ],
        ),
      ),


    );   
  }



  void startQuiz(){
   setState(() {
     Navigator.push(context, new MaterialPageRoute(builder: (context)=> new Quiz1()));
   });
  }



}
